#!/usr/bin/env sh

#########################################################################################
#                                                                                       #
# Check for updates and display desktop notification to logged in users                 #
#                                                                                       #
#########################################################################################

# Exit on any error
set -e

# Get all currently active users
USERS=$(who | awk '{print $1}' | sort | uniq)

# Get available updates
UPDATES="$(checkupdates)"  

# Display desktop notification 
if [[ ! -z "${UPDATES// }" ]]; then  
    for username in ${USERS}; do
        /bin/sudo -u ${username} DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/"$(id -u ${username})"/bus /usr/bin/notify-send "Updates found!" "${UPDATES}" --icon=dialog-information;  
    done
fi
