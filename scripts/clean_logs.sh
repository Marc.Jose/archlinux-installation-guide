#!/usr/bin/env sh

#########################################################################################
#                                                                                       #
# Delete user histories before each shutdown and logs older than 7 days                 #
#                                                                                       #
#########################################################################################

USER_DIRS=$(ls -d -1 /home/*; echo "/root")

for USER_DIR in ${USER_DIRS}; do
    # Delete user files
    rm --recursive --force \
        ${USER_DIR}/.cache \
        ${USER_DIR}/.thumbnails \
        ${USER_DIR}/.bash_history \
        ${USER_DIR}/.histfile \
        ${USER_DIR}/.lesshst \
        ${USER_DIR}/.mysql_history \
        ${USER_DIR}/.pgAdmin4.*.log \
        ${USER_DIR}/.python_history \
        ${USER_DIR}/.wget-hsts \
        ${USER_DIR}/.recently-used \
        ${USER_DIR}/.sqlite_history
        ${USER_DIR}/irclogs \
        ${USER_DIR}/armitage-tmp \
        ${USER_DIR}/.tmp/*
done

# Clear logs that are older than a week
find /var/log -type f -mtime +7 | xargs rm --force 

# Some permission fixes
if [[ ! -f "/var/log/pacman.log" ]]; then
    touch "/var/log/pacman.log"
fi
chmod o+r "/var/log/pacman.log"

