#!/usr/bin/env sh

# Restart some applications after suspend which otherwise might show some errors
# Save it under "/opt/scripts/wakeup.sh" and run "chmod u+xwr,g+xr,o+xr /opt/scripts/wakeup.sh"
# Then create a service-file under "/etc/systemd/system/wakeup.service" with the following content:
# [Unit]
# Description=Do some stuff after wakeup of hibernate and suspend
# After=suspend.target hibernate.target hybrid-sleep.target
#
# [Service]
# ExecStart=/bin/sh /root/scripts/system_wakeup.sh
#
# [Install]
# WantedBy=suspend.target hibernate.target hybrid-sleep.target
#
# And run "systemctl enable wakeup"

sleep 10s

killall plasmashell
killall latte-dock

sleep 5s

kstart5 plasmashell &
latte-dock &
