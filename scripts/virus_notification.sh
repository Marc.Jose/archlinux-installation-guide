#!/usr/bin/env sh
  
#########################################################################################
#                                                                                       #
# Display notfication if ClamAV found something suspicious                              #
#                                                                                       #
#########################################################################################

# Exit on any error
set -e

# Parameters
CLAM_VIRUSEVENT_VIRUSNAME=${1}
CLAM_VIRUSEVENT_FILENAME=$(grep -i "${CLAM_VIRUSEVENT_VIRUSNAME}" /var/log/clamav/clamd.log | tail -n 1 | awk '{print $(NF-2)}' | rev | cut -c 2- | rev)
CLAM_VIRUSEVENT_MESSAGE=$(echo -e "\nFound ${CLAM_VIRUSEVENT_VIRUSNAME} in:\n ${CLAM_VIRUSEVENT_FILENAME}\n")
CLAM_VIRUSEVENT_TITLE="clamAV - Alert"

# Get all currently active users with their screen
USERLIST=$(who | awk '{print $1$NF}' | sort | uniq)

# Display notification to all users
for entry in ${USERLIST}; 
do
    entry=(${entry/(/ })
    USERNAME=${entry[0]}
    DISPLAY=${entry[1]/)/}
    DBUS_ADDR="unix:path=/run/user/"$(id -u ${USERNAME})"/bus"
    
    /usr/bin/sudo -u ${USERNAME} \
        DISPLAY=${DISPLAY} \
        DBUS_SESSION_BUS_ADDRESS="${DBUS_ADDR}" \
        /usr/bin/notify-send \
            -i dialog-error \
            "${CLAM_VIRUSEVENT_TITLE}" \
            "${CLAM_VIRUSEVENT_MESSAGE}"
done
