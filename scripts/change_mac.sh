#!/usr/bin/env sh
  
#########################################################################################
#                                                                                       #
# Change MAC address for all network interfaces                                         #
#                                                                                       #
#########################################################################################

# Exit on any error
set -e

# Get a list of all interfaces and for each of them  
(ip -o link show | awk '{print $2}' | grep -vE '^lo|^vbox') | while read interface; do
    # Get interface name without the trailing :
    interface=${interface: : -1}
    
    # Get current MAC
    OLDMAC="$(macchanger --show ${interface} | head -n 1 | awk '{print $3}')"
    
    # Generate new MAC
    NEWMAC="$(macchanger --bia --random ${interface} | tail -n 1 | awk '{print $3}')"  
    
    # Disable interface
    ifconfig ${interface} down                                              > /dev/null  
    
    # Refresh interface with new MAC
    ifconfig ${interface} hw ether ${NEWMAC}                                > /dev/null  
    
    # Enable interface again
    ifconfig ${interface} up                                                > /dev/null     
    
    # Connect to internet
    dhcpcd ${interface}                                                     > /dev/null  2>&1          
done 
