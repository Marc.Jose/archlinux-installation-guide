###############################################################################################################################
#                                                                                                                             #
#   ZSH-Config                                                                                                                #
#   Provides several shell commands and usability features                                                                    #
#   Author: Marc Jose <marc@marc-jose.de>                                                                                     #
#                                                                                                                             #
###############################################################################################################################

# Set up history
HISTFILE="${HOME}/.histfile"
HISTSIZE=1000000
SAVEHIST=1000000
setopt HIST_IGNORE_DUPS
setopt HIST_REDUCE_BLANKS
setopt HIST_IGNORE_SPACE

# Define custom error function
function __error() { printf "%s\n" "$*" >&2; }

# Enable command not found hook by pkgfile
COMMANDNOTFOUND="/usr/share/doc/pkgfile/command-not-found.zsh"
if [[ -f "${COMMANDNOTFOUND}" ]]; 
then
    source "${COMMANDNOTFOUND}"
else
    __error "Command-Not-Found is not installed or cannot be found at '${COMMANDNOTFOUND}'!"
fi

# Autocompletion of command line switches for aliases
setopt COMPLETE_ALIASES

# Autocompletion with an arrow-key driven interface
zstyle ':completion:*' menu select 

# Autocompletion of privileged environments in privileged commands
zstyle ':completion::complete:*' gain-privileges 1

# Enable advanced autocompletion (ssh hostnames etc)
autoload -Uz compinit
compinit

# Enable history search
autoload -Uz history-beginning-search-backward history-beginning-search-forward
zle -N history-beginning-search-backward
zle -N history-beginning-search-forward

# Enable additional help commands
autoload -Uz run-help
autoload -Uz run-help-git
autoload -Uz run-help-ip
autoload -Uz run-help-openssl
autoload -Uz run-help-p4
autoload -Uz run-help-sudo
autoload -Uz run-help-svk
autoload -Uz run-help-svn

# Enable fish-like syntax highlighting
SYNTAXHIGHLIGHTING="/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
if [[ -f "${SYNTAXHIGHLIGHTING}" ]]; 
then
    source "${SYNTAXHIGHLIGHTING}"
else
    __error "ZSH-Syntax-Highlighting is not installed or cannot be found at '${SYNTAXHIGHLIGHTING}'!"
fi

# Enable auto suggestions
AUTOSUGGESTION="/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"
if [[ -f "${AUTOSUGGESTION}" ]]; 
then
    source "${AUTOSUGGESTION}"
    ZSH_AUTOSUGGEST_STRATEGY=(history completion)
    ZSH_AUTOSUGGEST_USE_ASYN=true
else
    __error "ZSH-Autosuggestions is not installed or cannot be found at '${AUTOSUGGESTION}'!"
fi
    
# Enable rehashing of new executables in $PATH
zstyle ':completion:*' rehash true

# Disable beep sound
setopt NO_BEEP

# Enable auto-cd if the typed argument is a directory
setopt AUTO_CD

# Enable auto-correction for commands
setopt CORRECT

# Enable extended globbing
setopt EXTENDED_GLOB

# Reset terminal state
ttyctl -f

# Enable keys
typeset -g -A key
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"
key[Control-Left]="${terminfo[kLFT5]}"
key[Control-Right]="${terminfo[kRIT5]}"
[[ -n "${key[Home]}"          ]] && bindkey -- "${key[Home]}"          beginning-of-line
[[ -n "${key[End]}"           ]] && bindkey -- "${key[End]}"           end-of-line
[[ -n "${key[Insert]}"        ]] && bindkey -- "${key[Insert]}"        overwrite-mode
[[ -n "${key[Backspace]}"     ]] && bindkey -- "${key[Backspace]}"     backward-delete-char
[[ -n "${key[Delete]}"        ]] && bindkey -- "${key[Delete]}"        delete-char
[[ -n "${key[Up]}"            ]] && bindkey -- "${key[Up]}"            up-line-or-beginning-search
[[ -n "${key[Down]}"          ]] && bindkey -- "${key[Down]}"          down-line-or-beginning-search
[[ -n "${key[Left]}"          ]] && bindkey -- "${key[Left]}"          backward-char
[[ -n "${key[Right]}"         ]] && bindkey -- "${key[Right]}"         forward-char
[[ -n "${key[PageUp]}"        ]] && bindkey -- "${key[PageUp]}"        beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"      ]] && bindkey -- "${key[PageDown]}"      end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}"     ]] && bindkey -- "${key[Shift-Tab]}"     reverse-menu-complete
[[ -n "${key[Control-Left]}"  ]] && bindkey -- "${key[Control-Left]}"  backward-word
[[ -n "${key[Control-Right]}" ]] && bindkey -- "${key[Control-Right]}" forward-word
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); 
then
    autoload -Uz add-zle-hook-widget
    function zle_application_mode_start { echoti smkx }
    function zle_application_mode_stop { echoti rmkx }
    add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
    add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

# Directory stack
autoload -Uz add-zsh-hook
DIRSTACKFOLDER="${XDG_CACHE_HOME:-$HOME/.cache}/zsh"
[[ ! -d ${DIRSTACKFOLDER} ]] && mkdir --parents ${DIRSTACKFOLDER}
DIRSTACKFILE="${DIRSTACKFOLDER}/dirs"
if [[ -f "$DIRSTACKFILE" ]] && (( ${#dirstack} == 0 )); 
then
    dirstack=("${(@f)"$(< "$DIRSTACKFILE")"}")
    [[ -d "${dirstack[1]}" ]] && cd -- "${dirstack[1]}"
fi
chpwd_dirstack() {
    print -l -- "$PWD" "${(u)dirstack[@]}" > "$DIRSTACKFILE"
}
add-zsh-hook -Uz chpwd chpwd_dirstack
DIRSTACKSIZE='100'
setopt AUTO_PUSHD PUSHD_SILENT PUSHD_TO_HOME
setopt PUSHD_IGNORE_DUPS
setopt PUSHD_MINUS

# Load 24bit colors
[[ "${COLORTERM}" == (24bit|truecolor) || "${terminfo[colors]}" -eq '16777216' ]] || zmodload zsh/nearcolor

# Environment variables
GPU_VENDOR=$(lspci -vnn | grep 'VGA compatible controller')
if $(echo "${GPU_VENDOR}" | grep -qi 'nvidia');
then
    export LIBVA_DRIVER_NAME=vdpau
    export VDPAU_DRIVER=nvidia
elif $(echo "${GPU_VENDOR}" | grep -Eiq 'amd|radeon');
then
    export LIBVA_DRIVER_NAME=radeonsi
    export VDPAU_DRIVER=va_gl
elif $(echo "${GPU_VENDOR}" | grep -iq 'intel');
then
    export LIBVA_DRIVER_NAME=i965
    export VDPAU_DRIVER=va_gl
fi
export SDL_VIDEO_X11_DGAMOUSE=0
export VISUAL='nano'
export SUDO_EDITOR='nano'
export EDITOR='nano'
export SSH_ASKPASS='qt4-ssh-askpass'
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_DATA_HOME="${HOME}/.local/share"
export PYTHONIOENCODING='UTF-8'
if [[ "$(loginctl show-session $(loginctl --value --property=Sessions show-user ${USER}) --value --property=Type)" == "wayland" ]];  
then  
    export MOZ_ENABLE_WAYLAND=1  
    export QT_QPA_PLATFORM='wayland'  
    export CLUTTER_BACKEND='wayland'  
    export SDL_VIDEODRIVER='wayland'  
fi  

# Setup anaconda
if [[ -d '/opt/anaconda/bin' ]];
then
    __conda_setup="$('/opt/anaconda/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
    if [ $? -eq 0 ]; then
        eval "$__conda_setup"
    else
        if [ -f "/opt/anaconda/etc/profile.d/conda.sh" ]; then
            . "/opt/anaconda/etc/profile.d/conda.sh"
        else
            export PATH="/opt/anaconda/bin:$PATH"
        fi
    fi
    unset __conda_setup
fi

# Run one and one SSH Agent only 
pgrep -u "${USER}" ssh-agent > /dev/null || ssh-agent > "${XDG_RUNTIME_DIR}/ssh-agent.env"
[[ "${SSH_AUTH_SOCK}" ]] || eval "$(<"${XDG_RUNTIME_DIR}/ssh-agent.env")"

# Build prompt style based on current directory, date and the last command
function __build_prompt {
    # Add last exit code
    local LPROMPT_STR="[%(?.%F{green}✔.%F{red}✗)%f]"
    
    # Add time
    LPROMPT_STR+="[%F{green}%D{%Y-%m-%d} %T%f]"
    
    # Add username and host
    LPROMPT_STR+="[%F{green}%n%f]"
    
    
    # Add current path
    local RPROMPT_STR='%B%F{cyan}%d%f%b'
    
    # Add git branch
    if [[ "$(git rev-parse --is-inside-work-tree 2> /dev/null)" == "true" ]]; then
        local GIT_BRANCH="$(git branch --show-current)"
        LPROMPT_STR+="[%F{cyan}${GIT_BRANCH}%f]"
    fi
    
    PROMPT="${LPROMPT_STR}: "
    RPROMPT="${RPROMPT_STR}"
}

# Rebuild prompt when changing directory
function chpwd {
    __build_prompt
}

# Create gitignore file using gitignore.io
function gitignore { 
    if [[ -z "$1" ]] || [[ "$1" == '-h' ]] || [[ "$1" == '--help' ]]; then
        local AVAILABLE_FORMATS="$(curl --silent --fail --location https://www.gitignore.io/api/list | tr ',' '\n' | column --fillrows)"
        echo -e "Available formats:\n${AVAILABLE_FORMATS}"
        echo -e 'Use with gitignore <comma separated list of templates> > .gitignore.'
    else
        curl --silent --location --write-out '\n' https://www.gitignore.io/api/$@
    fi 
}

# Set up a git repository at the current location
function gitsetup {
    # Params
    local git_email="$1"
    local git_name="$2"
    
    # Validate parameters
    if [[ -z "${git_name}" ]] || [[ "${git_email}" =~ '^[a-zA-Z0-9]\+@[a-zA-Z0-9]\+\.[a-z]\{2,\}' ]]; then
        echo -e 'Please provide a valid email to use and your name. (eg. gitsetup my@email.com "Eve Adam")'
        return 1
    fi
    
    # Check if the current folder is actually a git repository already otherwise initialize one
    if [[ ! -d '.git' ]]; then
        git init
    fi
    
    # Get GPG key if it exists for the given email otherwise generate new one
    gpg --list-secret-keys --keyid-format LONG "${git_email}" > /dev/null 2>&1
    if [[ $? -eq 2 ]]; then
        gpg --full-gen-key
    fi
    local gpg_key=$(gpg --list-secret-keys --keyid-format LONG "${git_email}" | grep 'sec' | head --lines=1 | awk '{print $2}' | cut --characters=9-)
    
    # Write actual project config
    cat <<EOF >> ".git/config"
[user]
        email = "${git_email}"
        name = "${git_name}"
        signingkey = "${gpg_key}"
[commit]
        gpgsign = true
[tag]
        forceSignAnnotated = true
[diff]
        renamelimit = 99999
[inspector]
        format = htmlembedded
        list-file-types = true
        metrics = true
        responsibilities = true
        timeline = true
        weeks = true
        hard = true
        grading = true
        file-types = **
[pull]
        rebase = true
[color]
        ui = true
[http]
        postBuffer = 524288000
EOF
}

# Welcome message containing a greeting with the current weather depending on the location based on your IP
# as well as some basic hardware information and update notifications
function __welcome {
    # Get current weather data
    local WEATHER_DATA="$(curl --insecure --connect-timeout 3 --max-time 2 --silent 'https://wttr.in' | awk '/┌/ {exit} {print}' 2> /dev/null)"
    local WEATHER='a'
    if [[ ${WEATHER_DATA} == *'sun'* ]]; then
        WEATHER+=' sunny'
    elif [[ ${WEATHER_DATA} == *'rain'* ]]; then
        WEATHER+=' rainy'
    elif [[ ${WEATHER_DATA} == *'cloud'* ]]; then
        WEATHER+=' cloudy'
    elif [[ ${WEATHER_DATA} == *'overcast'* ]]; then
        WEATHER+='n overcasted'
    elif [[ ${WEATHER_DATA} == *'fog'* ]]; then
        WEATHER+=' foggy'
    elif [[ ${WEATHER_DATA} == *'thunder'* ]]; then
        WEATHER+=' blasting'
    elif [[ ${WEATHER_DATA} == *'snow'* ]]; then
        WEATHER+=' winterly'
    elif [[ ${WEATHER_DATA} == *'clear'* ]]; then
        WEATHER+=' clear'
    fi
    
    # Kernel version
    local KERNEL="$(uname --kernel-release)"
    
    # Last time updates were checked
    if [[ -f /var/log/pacman.log ]];
    then
        local LASTCHECKED=$(tail --lines=1 /var/log/pacman.log | awk '{print $1}' | cut --characters=2- | rev | cut --characters=2- | rev)
        LASTCHECKED=$(date --date="${LASTCHECKED}" "+%Y-%m-%d %H:%M:%S %Z")
    fi
    if [[ -z "${LASTCHECKED}" ]]; then
        LASTCHECKED="Last session"
    fi
    if [[ "$(pacman --query --upgrades)" != '' ]]; then
        local UPDATES_AVAILABLE="($(pacman --query --upgrades | wc --lines) Update(s) available!)"
        LASTCHECKED+=" ${UPDATES_AVAILABLE}"
    fi
    
    # Hardare information
    local CPU="$(cat /proc/cpuinfo | grep --basic-regexp 'model name' | head --lines=1 | awk '{print $4 " " $5 " " $6 " " $7 " " $9 " " $10}' 2> /dev/null)"
    local GPU="$(lspci | grep ' VGA ' | grep --extended-regexp --only-matching '\[.*\]' | cut --characters=2- | rev | cut --characters=2- | rev)"
    local RAM="$(expr $(cat /proc/meminfo | grep 'MemTotal' | awk '{print $2}') / 1000 / 1000 2> /dev/null)"
    local MAC="$(macchanger --show $(ls /sys/class/net | awk '{print $1}') | head --lines=1 | awk '{print $3}')"
    local IP="$(curl --silent --connect-timeout 3  http://my.ip.fi/ || echo 'No connection')"

    # Weather depending greeting
    local GREETING="Welcome %n, what ${WEATHER}"

    # Time depending greeting
    local CURRENT_HOUR=$(date "+%H")
    if [[ ${CURRENT_HOUR} -lt 12 ]]; then 
        GREETING+=" morning!\n"
    elif [[ ${CURRENT_HOUR} -lt 18 ]]; then 
        GREETING+=" afternoon!\n"
    elif [[ ${CURRENT_HOUR} -lt 20 ]]; then 
        GREETING+=" evening!\n"
    else                       
        GREETING+=" night!\n"
    fi

    # Combine everything
    local MSG="${GREETING}"
    MSG+="\nSysteminformation:\n"
    MSG+=" Kernel:       ${KERNEL}\n"
    MSG+=" Last checked: ${LASTCHECKED}\n"
    MSG+=" CPU:          ${CPU}\n"
    MSG+=" GPU:          ${GPU}\n"
    MSG+=" RAM:          ${RAM} GB\n"
    MSG+=" IP:           ${IP}\n"
    MSG+=" MAC:          ${MAC}"

    print -P "${MSG}" | cowthink -f $(ls /usr/share/cows/*.cow | shuf --head-count=1) -W 500 -n
}


# Run functions on startup
__welcome
__build_prompt

# Aliases
# Aliases
# Go X directory(ies) up
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias .......='cd ../../../../../..'
# Get size of file or folder
alias size='du --summarize --total --human-readable 2> /dev/null'
# Display error/warning messages (all or just from today)
alias error-today='journalctl --no-hostname --quiet --boot --no-pager --no-tail --priority=3 --since="$(date +%Y-%m-%d) 00:00:00"'
alias error='journalctl --no-hostname --quiet --no-pager --no-tail --priority=3'
# Free unused memory/cache
alias freemem='printf "Clearing PageCache, dentries and inodes: "; \
               sudo sync; \
               echo 3 | sudo tee --append /proc/sys/vm/drop_caches; \
               printf "Clearing swap\n"; \
               sudo swapoff --all && \
               sudo swapon --all'
# List available updates
alias updates='pacman --query --upgrades'
# Update package mirror list
alias mirror-update='reflector -a $(date +%H) -c Germany -f 5 -p https --threads $(nproc --all) | sudo tee /etc/pacman.d/mirrorlist'
# Download updates via pacman and yay
alias update='sudo pacman -Syuq && \
              yay -Sua; \
              sudo updatedb; \
              sudo pkgfile --update'
# Fetch new mirror list and update as above, clean the package cache and remove unused packages
alias upgrade='mirror-update && \
               update && \
               pacclean && \
               autoremove'
# Clean package cache (keep only the latest versions)
alias pacclean='sudo paccache -rk 2'
# Remove unused packages
alias autoremove='pacman --query --deps --unrequired --quiet | sudo pacman --remove --recursive --noconfirm -'
# Get new configs (.pacnew)  
alias newconf='sudo find /etc -type f -name "*.pacnew"'  
# Display files/folders
alias lsh='ls -l --all --human-readable --color=auto --group-directories-first'
# Get current IP
alias myip='curl http://my.ip.fi/ || echo "Could not retrieve IP. Are you connected to the internet?"'
# Display network usage
alias iftop="sudo iftop -m 1024K -i $(route | grep 'default' | awk '{print $8}')"
# Display I/O usage
alias iotop='sudo iotop --only --processes'
# List disks with space usage
alias space='df --human-readable --print-type --exclude-type=tmpfs --exclude-type=devtmpfs'
# Retrieve random 64 character string via /dev/urandom
alias random='< /dev/urandom tr --delete --complement _A-Z-a-z-0-9 | head --bytes=${1:-64}; echo;'
# Check which package provides a command
alias whatprovides='pkgfile --search'
# Check who is using up all that swap
alias whoisusingmyswap="smem --columns='pid user command swap' --sort=swap --reverse | awk '\$NF != "0" {print}'"
# Get difference between files
alias diff='colordiff --side-by-side --suppress-common-lines'
# Get active connections
alias connections='sudo netstat --tcp --udp --listening --program --all --numeric'
# Get a list of all ToDo's and Fix(me)'s in a folder
alias todo='grep --recursive --line-number --extended-regexp "todo|\s+fix(me)\s+?"'
# Allow wget to continue downloads in case of an error
alias wget='wget --quiet --show-progress --continue'
# Create subdirectories as well if needed
alias mkdir='mkdir --parents --verbose'
# Run last command as root again
alias please="sudo !!"
# Copy using rsync
alias copy='rsync -recursive --links --perms --times --group --owner --devices --specials --hard-links --executability --progress --human-readable --checksum'
# Enable alias expansion for watch
alias watch='watch '
# Make grep colorful
alias grep='grep --color=auto'
# Instead of deleting everything right away use trash-cli ti move it to the trashcan
alias rm='trash-put'
# Empty trashcan
alias trash-empty='trash-empty; \
                   sudo trash-empty; \
                   sudo updatedb'
# Restart plasmashell if it got stuck
alias restartshell='killall plasmashell; kstart5 plasmashell &'
# Get fail2ban bans
alias fail2ban-log='sudo zgrep -h "Ban " /var/log/fail2ban.log* | awk '"'"'{print $NF}'"'"' | sort | uniq --count'
# Display temperature from CPU and Nvidia gpu
alias temperature='sensors | grep Package | sed -e '\''s/\+//g'\'' -e '\''s/\..*/\°C/g'\'' | awk '\''{print "CPU: " $4}'\''; echo "GPU: $(nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader,nounits || aticonfig --od-gettemperature)°C"'
# Check PKGBUILD and generate .SRCINFO before running makepkg to install it
alias build='namcap PKGBUILD && makepkg --printsrcinfo > .SRCINFO && makepkg --syncdeps --install'
# List docker images and containers
alias dockerinfo='echo "Images:"; \
                  sudo docker images; \
                  echo -e "\nContainers:"; \
                  sudo docker container ls -a; \
                  echo -e "\nRunning:"; \
                  sudo docker container ls;'
# Play music in given folder
alias music='vlc --quiet --qt-start-minimized --random --loop > /dev/null 2>&1'
# Find file duplicates
alias duplicates='rmlint --progress'
# Run backups
alias backup='rsync -recursive --links --perms --times --group --owner --devices --specials --hard-links --executability --progress --human-readable --checksum --delete --verbose'
alias backup-dry='rsync -recursive --links --perms --times --group --owner --devices --specials --hard-links --executability --progress --human-readable --checksum --delete --verbose --dry-run'
# Help command
alias help=run-help
# List running services
alias running-services='systemctl list-units | grep --extended-regexp "UNIT.*LOAD.*ACTIVE.*SUB.*DESCRIPTION|running"'
# Compress all video files in folder using x265
alias mov-compress='for mov in *.mp4; do ffmpeg -hide_banner -loglevel warning -i "${mov}" -c:v libx265 -crf 25 -c:a aac -b:a 128k $(basename "${mov}" ".mp4")_compr.mp4; done'
# Compress all video files in folder using nvidia encode and hevc
alias nv-mov-compress='for mov in *.mp4; do ffmpeg -hide_banner -loglevel warning -i "${mov}" -preset slow -c:v hevc_nvenc -c:a aac -b:a 128k $(basename "${mov}" ".mp4")_compr.mp4; done'
# Scan all ports; TCP ports; UDP ports; OS/Service detection; Service/Daemon versions
alias full-scan='sudo nmap -p 1-65535 -sT -sU -A -sV '
# Scan all ports; TCP ports; UDP ports
alias quick-scan='sudo nmap -p 1-65535 -sT -sU '
# Scan all ports; TCP ports; UDP ports; CVE detection; Malware detection; Google malware detection
alias vuln-scan='sudo nmap -p 1-65535 -sT -sU -Pn --script vuln -sV --script=http-malware-host --script http-google-malware '
# Visualize repository activity
alias gource='gource --seconds-per-day 1 --key --auto-skip-seconds 1 --user-image-dir "$(git rev-parse --show-toplevel)/.git/avatar"'
