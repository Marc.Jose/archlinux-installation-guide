# Archlinux Example Installation
This wiki describes an example setup for an [Archlinux](https://www.archlinux.org/) installation. By no means it wants to be perfect or be the only way of doing it. For more setup hints you might take a look [here](https://wiki.archlinux.org/index.php/Installation_guide). 
You decide what your system should be like!

## Table Of Contents
0. [Create Bootable Stick](#0-create-bootable-stick)
1. [Key Layout](#1-key-layout)  
2. [Internet Connectivity](#2-internet-connectivity)  
3. [Clock](#3-clock)
4. [Partitions](#4-partitions)
5. [Encrypting](#5-encrypting)
6. [Formatting](#6-formatting)
7. [Mounting Partitions](#7-mounting-partitions)
8. [Swapfile](#8-swapfile)
9. [Installing Base](#9-installing-base)
10. [Fstab](#10-fstab)  
11. [Hostname](#11-hostname)  
12. [Timezone](#12-timezone)  
13. [Locales](#13-locales)  
14. [Pacman](#14-pacman)  
15. [User Management](#15-user-management)  
16. [AUR](#16-aur)
17. [Create Initial Ramdisk](#17-create-initial-ramdisk)    
18. [Bootloader](#18-bootloader)   
19. [System Configuration](#19-system-configuration)  
    19.1 [Swappiness](#191-swappiness)  
    19.2 [Logging](#192-logging)   
    19.3 [Fancy Boot](#193-fancy-boot)   
20. [Security](#20-security)   
    20.1 [Process Limits](#201-process-limits)  
    20.2 [Restrict Login](#202-restrict-login)   
    20.3 [SSH Settings](#203-ssh-settings)  
    20.4 [TCP/IP Stack Hardening](#204-tcpip-stack-hardening)  
    20.5 [UMASK](#205-umask)  
    20.6 [IP SPoofing](#206-ip-spoofing)  
    20.7 [Legal Banner](#207-legal-banner)   
    20.8 [USB Protection](#208-usb-protection)  
    20.9 [DNSCrypt](#209-dnscrypt)  
    20.10 [Telemetry](#2010-telemetry)   
    20.11 [Antivirus](#2011-antivirus)   
    20.12 [Firewall](#2012-firewall)  
21. [Enable Services](#21-enable-services)  
22. [Update Databases](#22-update-databases)   
23. [Update Notifications](#23-update-notfications)  
24. [Finishing](#24-finishing)  

## 0. Create Bootable Stick
Download the ISO, signature and hash file from any mirror found at https://www.archlinux.org/download/ and verify their integrity.  

```shell
# Download all files (ISO, signature and hash)
$ wget "https://arch.jensgutermuth.de/iso/2021.01.01/archlinux-2021.01.01-x86_64.iso"
$ wget "https://arch.jensgutermuth.de/iso/2021.01.01/sha1sums.txt"
$ wget "https://arch.jensgutermuth.de/iso/2021.01.01/archlinux-2021.01.01-x86_64.iso.sig"
    
# Compare the SHA1 hash with the one found at https://archlinux.org/download/ (c3082b13d3cf0a253e1322568f2fd07479f86d52 in this case)
$ sha1sum --ignore-missing -c sha1sums.txt 
    
# Verify signatures
$ gpg --quiet --keyserver-options auto-key-retrieve --verify archlinux-2021.01.01-x86_64.iso.sig archlinux-2021.01.01-x86_64.iso
```
	
If everything looks okay you can plug in your USB drive and get its device path with `lsblk`.   

```shell
$ lsblk 
NAME       MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda          8:0    0 465.8G  0 disk  
├─sda1       8:1    0  1023M  0 part  /boot
└─sda2       8:2    0 464.8G  0 part  
  └─arch   253:0    0 464.7G  0 crypt / 
sdd          8:48   1   964M  0 disk  # This is the stick in our example
├─sdd1       8:49   1   647M  0 part  
└─sdd2       8:50   1    64M  0 part  
```

If your stick is mounted already, run `umount /dev/sdX` before continuing. Then use `dd` to write the image to the USB stick where `/dev/sdX` is the corresponding drive path. 

```shell
$ dd bs=4M if=archlinux-2021.01.01-x86_64.iso of=/dev/sdX conv=fsync oflag=direct status=progress
```

When finished reboot your computer and use the newly created stick as the boot drive (usually it requires you to press a key like `Del` or `F8` but refer to your computers manual for more details). You will then be greeted with a commandline as soon as everything is ready. From there you can start with the installation process.

## 1. Key Layout  
As the first step you should change the keylayout if needed. By default an american one is used. So if you prefer the german one for example you can switch to it using the command 

```shell
$ loadkeys de-latin1
```

You can find all available layouts by running

```shell
$ localectl list-keymaps
``` 

## 2. Internet Connectivity
To download and install packages you will need a working internet connection. The easiest way to do so is by plugging in a network cable and checking via:

```shell
$ ping -c 1 archlinux.org 
PING archlinux.org(archlinux.org (2a01:4f9:c010:6b1f::1)) 56 data bytes
64 bytes from archlinux.or`` (2a01:4f9:c010:6b1f::1): icmp_seq=1 ttl=54 time=41.0 ms

--- archlinux.org ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 40.982/40.982/40.982/0.000 ms
```

For WiFi connections please refer to the manual for [iwctl](https://wiki.archlinux.org/title/Iwd#iwctl)

## 3. Clock  
Update system clock to avoid problems later on with the package manager via the command 

```shell
$ timedatectl set-ntp true
```

## 4. Partitions
Assuming you start with an empty disk we generate two partitions in the following example. One will contain the boot files and the other is the root partition of your actual system. So again as in step 0 use the command `lsblk` to find the appropriate path of your drive. This should be something like `/dev/sdX`. Also you need to check if you are using UEFI or BIOS which will make a difference during installation. You can do so by running

```shell
$ ls /sys/firmware/efi/efivars
```

If the folder exists you are using UEFI otherwise it is BIOS. Depending on that the following commands will be different.

```shell
$ parted -ms /dev/sdX unit MiB
    
# If UEFI is used
$ parted -ms /dev/sdX mklabel gpt
$ parted -ms /dev/sdX mkpart primary fat32 0% 1024MiB
$ parted -ms /dev/sdX set 1 esp on
    
# If BIOS is used
$ parted -ms /dev/sdX mklabel msdos
$ parted -ms /dev/sdX mkpart primary ext4 0% 1024MiB
$ parted -ms /dev/sdX set 1 boot on
    
# For both types
$ parted -ms /dev/sdX mkpart primary ext4 1024MiB 100%
```
    
## 5. Encrypting
To encrypt your root partition which is highly recommended you can use [dm-crypt](https://wiki.archlinux.org/index.php/Dm-crypt). You can skip this step if you don't need it but the steps afterwards assume it is used. Proceed by running:

```shell
$ modprobe dm-crypt
# The parameter for i should be a random number higher or equal to 2000   
$ cryptsetup -s 512 -h sha512 luksFormat -i 7096 /dev/sdX2
$ cryptsetup open /dev/sdX2 ARCHLINUX
```

## 6. Formatting
Now you can define the file system type which should be used. 
This again depends whether you are using [UEFI](https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface) or BIOS as well as if the root partition was encrypted.

```shell
$ mkfs.ext4 -L ARCHLINUX /dev/mapper/ARCHLINUX

# If UEFI is used
$ mkfs.fat -F 32 -n BOOT /dev/sdX1
    
# If BIOS is used
$ mkfs.ext4 -L BOOT /dev/sdX1
```

## 7. Mounting Partitions  
Next mount all partitions so you can start installing the system:  

```shell
$ mount /dev/mapper/archlinux /mnt

$ mkdir /mnt/boot
$ mount /dev/sdX1 /mnt/boot
```

## 8. Swapfile
The [swap](https://wiki.archlinux.org/index.php/Swap) is used to either extend your existing physical memory as well as when suspending the system to store the content of your memory in it. You can either use your own swap partition or a swap file. A swap file allows easier resizing and is therefore recommended. To create a swapfile of the same size as your memory use the commands

```shell
$ dd if=/dev/zero of=/mnt/swap bs=1M count=$(echo $(grep "MemTotal" /proc/meminfo | awk '{print $2}') / 1000 | bc)
$ chmod 600 /mnt/swap
$ mkswap -L SWAP /mnt/swap
$ swapon /mnt/swap
```

## 9. Installing Base  
Now that the whole filesystem is ready you can start installing the base system via

```shell
$ pacstrap /mnt base base-devel vim nano wget git colordiff cronie mlocate grub efibootmgr os-prober dialog sudo linux linux-firmware pacman-contrib reflector intel-ucode ...
```
You might want to add additional packages to the command. This can take quite a while depending on your network connection. If you are running an AMD CPU you also need to swap `intel-ucode` with `amd-ucode`. You can find out the manufacturer by running

```shell
# CPU:
$ grep 'model name' /proc/cpuinfo

# GPU:
$ lspci -vnn | grep 'VGA compatible controller'
```

Example packages that can also be installed:

> **Base Install:** cowsay cpupower dhcpcd htop iftop iotop iw lm_sensors man-db net-tools networkmanager-openconnect pkgfile powertop pv quota-tools realtime-privileges reflector rfkill rmlint rrdtool screen smem thermald trash-cli wine wine-gecko wine-mono winetricks wireless_tools wpa_supplicant zsh zsh-completions zsh-syntax-highlighting zsh-autosuggestions   
> **Security:** clamav dnscrypt-proxy keepassxc ksshaskpass nftables nmap openssh openssh-askpass usbguard veracrypt wireshark-qt macchanger       
> **Desktop:** dolphin latte-dock plasma firefox thunderbird bluez-utils  
> **Multimedia:** brasero cheese ffmpeg ffmpegthumbnailer ffmpegthumbs gwenview vlc inkscape pipewire-pulse pipewire-alsa pipewire-jack        
> **Messenger:** konversation signal-desktop teamspeak3 neochat   
> **Games:** lutris steam steam-native-runtime vkd3d vulkan-icd-loader lib32-fontconfig ttf-liberation wqy-zenhei  
> **Utils:** ark gparted gwenview kdeconnect nextcloud-client simplescreenrecorder spectacle yakuake   
> **Developer:** gradle python python-pip python-virtualenv virtualbox virtualbox-ext-vnc virtualbox-guest-iso qt qtcreator   
> **Office:** cups cups-filters cups-pdf kate kile krita libreoffice okular texlive-most xsane    
> **Hardware Acceleration:** libva-utils libvdpau-va-gl libva-vdpau-driver vdpauinfo  
> **GPU (Nvidia):** nvidia nvidia-settings lib32-nvidia-utils   	 
> **GPU (AMD):** mesa lib32-mesa xf86-video-amdgpu vulkan-radeon mesa-vdpau lib32-amdvlk libva-mesa-driver mesa-vdpau    
> **GPU (Intel):** intel-media-driver libva-intel-driver vulkan-intel xf86-video-intel lib32-mesa lib32-vulkan-intel   

## 10. Fstab  
To let your system remember which partitions should be mounted on start you need to create the [fstab](https://wiki.archlinux.org/index.php/Fstab) with the following command:

```shell
$ genfstab -t UUID -p /mnt > /mnt/etc/fstab
```

Also you should increase the size of your `tmp` folder in order to build larger packages. This can be done by adding the line:

> \# tmpfs
> tmpfs /tmp tmpfs rw,nodev,nosuid,size=&lt;MEM_SIZE&gt; 0 0

to `/mnt/etc/fstab`.

## 11. System Name
Next you should chose a name for your computer with the command:

```shell
$ echo <HOSTNAME> > /mnt/etc/hostname  
```

and append

> 127.0.0.1       localhost  
> ::1             localhost  
> 127.0.1.1       \<HOSTNAME\>.localdomain        \<HOSTNAME\>  

to `/mnt/etc/hosts`.

## 12.  Timezone
Define your local timezone using the command

```shell
$ arch-chroot /mnt timedatectl set-timezone <REGION>
```

A list of available timezones can be aquired with

```shell
$ timedatectl list-timezones
```

and set the hardware clock accordingly:

```shell
$ arch-chroot /mnt hwclock --systohc
```

## 13. Locales
Next set up the language of your system and the default keymap. The list of languages/fonts supported can be retrieved via the command

```shell
$ cat /etc/locale.gen

$ find /usr/share/kbd/consolefonts -type f -name \*.psfu\.gz -exec basename {} \;
```

Then edit the file `/mnt/etc/locale.conf`and add the following content:

> LANG=en_GB.UTF-8 &num;Example: en_GB is the selected language  
> LANGUAGE=en_GB.UTF-8  
> LC_NUMERIC=en_GB.UTF-8  
> LC_MESSAGES=en_GB.UTF-8  
> LC_NAME=en_GB.UTF-8  
> LC_ADDRESS=en_GB.UTF-8  
> LC_TELEPHONE=en_GB.UTF-8  
> LC_IDENTIFICATION=en_GB.UTF-8   
> LC_MONETARY=en_GB.UTF-8    
> LC_COLLATE=en_GB.UTF-8    
> LC_CTYPE=en_GB.UTF-8    
> LC_TIME=de_DE.UTF-8 &num;24 hour system    
> LC_PAPER=de_DE.UTF-8 &num;A4,..   
> LC_MEASUREMENT=de_DE.UTF-8 &num;Metric system    

Now uncomment your desired languages in the file `/mnt/etc/locale.gen` and run

```shell
$ arch-chroot /mnt locale-gen
```

Finally add to `/mnt/etc/vconsole.conf`as below. 

> KEYMAP=de-latin1 &num; The keymap you want to use for your system  
> FONT=eurlatgr  

## 14. Pacman
Next you can configure the default package manager of Archlinux which is [pacman](https://wiki.archlinux.org/index.php/Pacman). To do so open the file at `/mnt/etc/pacman.conf` and make sure the following lines are uncommented or added:

> [options]  
> HookDir = /etc/pacman.d/hooks  
>      
> &num; Misc options 
> Color   
> VerbosePkgLists  
> ILoveCandy  
>      
> [multilib]  
> Include = /etc/pacman.d/mirrorlist  

Finally create the folder `/mnt/etc/pacman.d/hooks` with

```shell
$ mkdir /mnt/etc/pacman.d/hooks  
```

This will enable colorized outputs, overall download progess, more verbose package overview as well as activating the multilib repository which contains 32bit software. Then update the package database with the command

```shell
$ arch-chroot /mnt pacman -Sy
```

Since GPU drivers need some special treatment add the file `/mnt/etc/pacman.d/hooks/gpu.hook`:
> [Trigger]  
> Operation=Install  
> Operation=Upgrade  
> Operation=Remove  
> Type=Package    
> Target=nvidia  
> Target=amdgpu    
> Target=mesa   
> Target=linux*   
>        
> [Action]  
> Description=Update gpu modules in initcpio  
> Depends=mkinitcpio     
> When=PostTransaction   
> NeedsTargets  
> Exec=/bin/sh -c 'while read -r trg; do case $trg in linux) exit 0; esac; done; /usr/bin/mkinitcpio -P'    

## 15. User Management
Now it is time to add new user accounts and protect the root account with a password. This can be done via

```shell
$ arch-chroot /mnt passwd
```

Then we can start adding our custom users with

```shell
$ arch-chroot /mnt useradd -mG wheel,video,audio,optical,storage,realtime,vboxusers <USERNAME>  
# Download a sample ZSH config
$ wget -O "/mnt/home/<USERNAME>/.zshrc" "https://gitlab.com/marcjose/archlinux/installation-guide/-/raw/main/configs/.zshrc"
$ arch-chroot /mnt passwd <USERNAME> 
``` 

If you want this user to be able to use `sudo` you have to allow that by adding

> &lt;Username&gt; ALL=(ALL) ALL  
    
to `/mnt/etc/sudoers`. These steps can be repeated as many times as you want.
Alternatively you can uncomment the line

> %wheel ALL=(ALL) ALL   

to allow all members of the wheel group access.

**Attention:** Use strong passwords with atleast 16 characters. Especially for the root user as having access to that account means having access to everything!

## 16. AUR
The [AUR](https://aur.archlinux.org/) provides packages made by other users. These packages are not official so manual checking the sources is recommended. To make installation easier there are so called `AUR manager` tools. One of them, `yay` can be installed via:

```shell
$ arch-chroot /mnt sh -c "git clone https://aur.archlinux.org/yay.git /tmp/yay; chown -R <USERNAME> /tmp/yay; cd /tmp/yay; sudo -Hu <USERNAME> makepkg -si"
```

Packages can then be installed with:

```shell
$ arch-chroot /mnt sh -c "sudo -Hu <USERNAME> yay -S [...]"
```

Example package lists:

> **Base Install:** plymouth      
> **Security:** clamav-unofficial-sigs tor-browser        
> **Multimedia:** spotify  
> **Messenger:** rambox-bin  
> **Games:** protontricks proton-ge-custom-bin steam-fonts    
> **Developer:** jetbrains-toolbox unityhub virtualbox-ext-oracle   
> **Office:** samsung-unified-driver       

## 17. Create Initial Ramdisk
Now you can create the [initial ramdisk](https://wiki.archlinux.org/index.php/Mkinitcpio) by modifying the file `/mnt/etc/mkinitcpio.conf`:

> &num; If an nvidia gpu is used  
> MODULES=(vfat ext4 nvidia nvidia_modeset nvidia_uvm nvidia_drm)  
> 	  
> &num; If an amd gpu is used  
> MODULES=(vfat ext4 amdgpu radeon)  
>  
> &num; If an intel gpu is used  
> MODULES=(vfat ext4 i915 intel_agp) 
>   
> HOOKS=(base udev plymouth autodetect keyboard keymap consolefont modconf block plymouth-encrypt mdadm_udev filesystems resume fsck)  

Then run the command

```shell
$ arch-chroot /mnt mkinitcpio -P
```
   
## 18. Bootloader
Next you should configure [grub](https://wiki.archlinux.org/index.php/Grub) by modifying `/mnt/etc/default/grub`:

> GRUB_DEFAULT=saved  
> GRUB_CMDLINE_LINUX_DEFAULT="audit=0 loglevel=3 iommu=soft rw quiet rd.systemd.show_status=auto rd.udev.log_priority=3 vt.global_cursor_default=0 splash"  
> GRUB_CMDLINE_LINUX="cryptdevice=UUID=&lt;CRYPTO_ROOT_UUID&gt;:archlinux root=/dev/mapper/archlinux resume=UUID=&lt;ROOT_UUID&gt; resume_offset=&lt;SWAP_OFFSET&gt;  
> GRUB_ENABLE_CRYPTODISK=y  
> GRUB_DISABLE_SUBMENU=y  
> GRUB_SAVEDEFAULT=true  

The UUID can be retrieved via:

```shell
# CRYPTO_ROOT_UUID
$ blkid -o value -s UUID /dev/sdX2
# ROOT_UUID
$ blkid -o value -s UUID /dev/mapper/archlinux
# SWAP_OFFSET
$ filefrag -v /mnt/swap | awk '{if($1=="0:"){print $4}}' | rev | cut -c 3- | rev
```

Then run depending if you use UEFI or BIOS:
    
```shell
# UEFI
$ arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
$ arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
$ arch-chroot /mnt efibootmgr --disk /dev/sdX --part 1 --create --label "ArchLinux" --loader /vmlinuz-linux --unicode "initrd=\intel-
ucode.img initrd=\initramfs-linux.img cryptdevice=UUID=<CRYPTO_ROOT_UUID>:ARCHLINUX root=/dev/mapper/ARCHLINUX resume=UUID=<ROOT_UUID> resume_offset=<SWAP_OFFSET> audit=0 loglevel=3 iommu=soft rw quiet rd.systemd.show_status=auto rd.udev.log_priority=3 vt.global_cursor_default=0 splash" --verbose

# BIOS
$ arch-chroot /mnt grub-install /dev/sdX
$ arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
```
        
**Hint:** Make sure to swap `intel-ucode` with `amd-ucode` if you are using an AMD CPU. 

## 19. System Configuration
### 19.1 Swappiness
Using the swap slows down the whole system tremendously so you probably only want it to be used if it cannot be avoided. This can be accomplished by adding

> vm.swappiness=1  
    
to `/mnt/etc/sysctl.d/99-sysctl.conf`

### 19.2 Logging
Unfortunately Plasma is logging way too much per default especially debug stuff but we can disable some of it by adding  

> QT_LOGGING_RULES='&ast;.debug=false'  

to `/mnt/etc/environment`.

### 19.3 Fancy Boot
In order to get a nice graphical boot theme you can download the theme [here](https://gitlab.com/marcjose/archlinux/plymouth-theme-archlinux):

```shell
$ wget -O /tmp/plymouth.tar.gz "https://gitlab.com/marcjose/archlinux/plymouth-theme-archlinux/-/archive/main/plymouth-theme-archlinux-main.tar.gz"
$ tar -xzf /tmp/plymouth.tar.gz  
$ mv plymouth-theme-archlinux-main /mnt/usr/share/plymouth/themes/archlinux
$ chmod g+rx,o+rx /mnt/usr/share/plymouth/themes/archlinux
$ chmod u-x,g+r,o+r /mnt/usr/share/plymouth/themes/archlinux/*
$ arch-chroot /mnt plymouth-set-default-theme -R archlinux
```

There is also a matching Grub theme available [here](https://gitlab.com/marcjose/archlinux/grub-theme-archlinux) which you can install by running:
```shell
$ wget -O /tmp/grub.tar.gz "https://gitlab.com/marcjose/archlinux/grub-theme-archlinux/-/archive/main/grub-theme-archlinux-main.tar.gz"
$ tar -xzf /tmp/grub.tar.gz  
$ mv grub-theme-archlinux-main /mnt/boot/grub/themes/
$ chmod g+rx,o+rx /mnt/boot/grub/themes/archlinux-*
$ chmod g+r,o+r /mnt/boot/grub/themes/archlinux-*/*
```
Then add the following line (adapt depending on your local screen resolution):

> GRUB_THEME="/boot/grub/themes/archlinux-hd/theme.txt"  

to `/mnt/etc/default/grub` and re-run `arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg`.

## 20. Security
### 20.1 Process Limits
Limit the amount of processes for each user in `/mnt/etc/security/limits.conf`by adding

> &ast;               soft    nproc           2000  
> &ast;               hard    nproc           2000  

as well as in `/mnt/etc/systemd/system.conf`

> DefaultLimitNPROC=2000:2000

### 20.2 Restrict Login
Restrict  (root) login via terminal in `/mnt/etc/pam.d/su` and `/mnt/etc/pam.d/su-l` by uncommenting the line

> auth   required   pam_wheel.so use_uid  

as well as commenting all tty's in `/mnt/etc/securetty`:

> &num;tty1  
> &num;tty2  
> &num;tty3  
> ...  

### 20.3 SSH Settings
Next harden your [SSH](https://wiki.archlinux.org/index.php/Secure_Shell) settings at `/mnt/etc/ssh/sshd_config` if you installed the `openssh` package. The following modifications will deny root login, force key usage and allow only whitelisted users:

> PermitRootLogin no  
> PasswordAuthentication no  
> AllowUsers &lt;USERNAME&gt;  

Also if you want to make use of the SSH-agent (which is supported by KeePassXC) create a file at `/mnt/home/<USERNAME>/.config/systemd/user/ssh-agent.service` with content:

> [Unit]  
> Description=SSH key agent  
>   
> [Service]  
> Type=simple  
> Environment=SSH_AUTH_SOCK=%t/ssh-agent.socket  
> ExecStart=/usr/bin/ssh-agent -D -a ${SSH_AUTH_SOCK}  
>   
> [Install]  
> WantedBy=default.target  

as well as adding  

> SSH_AUTH_SOCK DEFAULT="${XDG_RUNTIME_DIR}/ssh-agent.socket"  

to `/mnt/homt/<USERNAME>/.pam_environment` before running `arch-chroot /mnt sh -c "sudo -Hu <USERNAME> systemctl --user enable ssh-agent.service"`.  

**Hint**: You might have to create the folder first by running

```shell
mkdir -p /mnt/home/<USERNAME>/.config/systemd/user
arch-chroot /mnt chown -R <USERNAME>:<USERNAME> /home/<USERNAME>/.config/systemd/user/ssh-agent.service
```

### 20.4 TCP/IP Stack Hardening
Next you can harden your TCP/IP stack in `/mnt/etc/sysctl.d/51-net.conf` by adding:

> \# IP Spoofing protection  
> net.ipv4.conf.all.rp_filter = 1  
> net.ipv4.conf.default.rp_filter = 1  
>   
> \# Ignore ICMP broadcast requests  
> net.ipv4.icmp_echo_ignore_broadcasts = 1  
>   
> \# Disable source packet routing  
> net.ipv4.conf.all.accept_source_route = 0  
> net.ipv6.conf.all.accept_source_route = 0  
> net.ipv4.conf.default.accept_source_route = 0  
> net.ipv6.conf.default.accept_source_route = 0  
>   
> \# Ignore send redirects  
> net.ipv4.conf.all.send_redirects = 0  
> net.ipv4.conf.default.send_redirects = 0  
>   
> \# Block SYN attacks  
> net.ipv4.tcp_syncookies = 1  
> net.ipv4.tcp_max_syn_backlog = 2048  
> net.ipv4.tcp_synack_retries = 2  
> net.ipv4.tcp_syn_retries = 5  
>   
> \# Log Martians  
> net.ipv4.conf.all.log_martians = 1  
> net.ipv4.icmp_ignore_bogus_error_responses = 1  
>   
> \# Ignore ICMP redirects  
> net.ipv4.conf.all.accept_redirects = 0  
> net.ipv6.conf.all.accept_redirects = 0  
> net.ipv4.conf.default.accept_redirects = 0  
> net.ipv6.conf.default.accept_redirects = 0  
>   
> \# Ignore Directed pings  
> net.ipv4.icmp_echo_ignore_all = 1  

### 20.5 UMASK
To prevent other users from accessing your files you can tighten the UMASK in `/mnt/etc/profile`

> umask 0077  

### 20.6 IP-Spoofing
Prevent IP-spoofing in `/mnt/etc/hosts` by adding

> order bind,hosts  

at the top.

### 20.7 Legal Banner
Add a legal banner in `/mnt/etc/issue` which will be shown when connecting to the system via SSH. You can download an example via:

```shell
$ wget -O "/mnt/etc/issue" "https://gitlab.com/marcjose/archlinux/installation-guide/raw/main/configs/issue"
```

### 20.8 USB Protection
To prevent malicious USB devices to be mounted to your system you can use `usbguard`. Don't forget to add your username to the config at `/mnt/etc/usbguard/usbguard-daemon.conf`and change the default policy, otherwise you might find yourself with non-working devices at startup:

> IPCAllowedUsers=root &lt;Username&gt;  
> PresentDevicePolicy=allow  

### 20.9 DNSCrypt 
To encrypt your DNS requests you can use `dnscrypt-proxy`. After installing edit `/mnt/etc/dnscrypt-proxy/dnscrypt-proxy.toml` to enable the DNSSEC option:

> listen_addresses = ['127.0.0.1:53', '[::1]:53']  
> ipv6_server = true
> require_dnssec = true  

Then edit `/mnt/etc/resolv.conf.head` and add  

> &num; Use dnscrypt resolver first  
> nameserver 127.0.0.1  
> options edns0 single-request-reopen  
>  
> &num; Fallback servers  
> &num;&num; Google  
> nameserver 8.8.8.8  
> nameserver 8.8.4.4  
> nameserver 2001:4860:4860::8888  
> nameserver 2001:4860:4860::8844  
> &num;&num; Cloudflare  
> nameserver 1.1.1.1  
> nameserver 2606:4700:4700::1111  
>   

and run `chattr +i /mnt/etc/resolv.conf.head` to prevent changes on this file.

### 20.10 Telemetry  
Currently NetworkManager makes use of the Archlinux servers to check for connectivity. This happens kinda every now and then. Apparently this is not being logged ([1](https://git.archlinux.org/infrastructure.git/commit/?id=68fbaca2ef9f31f624f117899848f4288d6b39d1)) but can still be used for telemetry data like when a computer is on and who is using it. So to disable it if you are using [NetworkManager](https://wiki.archlinux.org/index.php/NetworkManager) add to `/mnt/etc/NetworkManager/NetworkManager.conf`: 

> [connectivity]  
> &num;uri=http://www.archlinux.org/check_network_status.txt  
> interval=0  

You can also use your own server to check here instead if you still like to have the feature.  

### 20.11 Antivirus
As malware is a problem even on UNIX systems you should definitely have an antivirus application. For example [ClamAV](https://wiki.archlinux.org/index.php/ClamAV) for which you can enable on access scanning by replacing the `config` value in `/mnt/usr/lib/systemd/system/clamav-clamonacc.service`:

> --config-file=/etc/clamav/clamd.conf  

and setting the config parameters in `/mnt/etc/clamav/clamd.conf`:

> \# This needs to be excluded currently, otherwise the system gets spammed with error messages  
> OnAccessExcludePath /proc   
> OnAccessExcludePath /tmp  
> OnAccessExcludePath /sys  
> OnAccessExcludePath /run  
> \# Do not scan files accessed by the scanner again  
> OnAccessExcludeUname clamav  
> OnAccessExtraScanning yes  
> \# All paths that should be monitored  
> OnAccessIncludePath /boot   
> OnAccessIncludePath /dev   
> OnAccessIncludePath /etc   
> OnAccessIncludePath /home   
> OnAccessIncludePath /mnt  
> OnAccessIncludePath /opt  
> OnAccessIncludePath /root  
> OnAccessIncludePath /run   
> OnAccessIncludePath /srv   
> OnAccessIncludePath /sys  
> OnAccessIncludePath /tmp   
> OnAccessIncludePath /usr  
> OnAccessIncludePath /var  
> \# Run as root to scan all files  
> User root  
> \# Trigger this script when a virus was found  
> VirusEvent /usr/bin/bash /etc/clamav/detected.sh %v  

To get desktop notifications if a virus was found you can download the script found [here](https://gitlab.com/marcjose/archlinux/installation-guide/-/raw/main/scripts/virus_notification.sh):

```shell
$ wget -o /mnt/etc/clamav/detected.sh "https://gitlab.com/marcjose/archlinux/installation-guide/-/raw/main/scripts/virus_notification.sh"
```

### 20.12 Firewall
A firewall is needed to block unwanted packages. Currently [nftables](https://wiki.archlinux.org/index.php/Nftables) seems to be the way to go. You can configure it by altering `/mnt/etc/nftables.conf`:

> \#!/usr/bin/nft -f      
> \# Flush rule set  
> flush ruleset     
> \# Define IPs to be blocked no matter what  
> \#define BLOCKED_IPS = { }    
> table inet filter {  
> &nbsp;&nbsp;&nbsp;&nbsp;chain input {  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# By default drop everything, unless specified otherwise  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type filter hook input priority 0; policy drop;       
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Drop invalid connections  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ct state invalid drop  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Drop connections from banned IPs  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\#ip saddr \$BLOCKED_IPS drop  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Accept established/related connections  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ct state { established, related } accept  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Accept from loopback  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iif lo accept       
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Accept icmp(6)  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ip protocol icmp limit rate 1/second accept  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ip6 nexthdr ipv6-icmp limit rate 1/second accept     
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Accept kdeconnect  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tcp dport 1714-1764 accept  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;udp dport 1714-1764 accept       
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Count and log other traffic  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#log prefix "[nftables] Inbound Denied: " flags all limit rate 1/minute counter drop  
> &nbsp;&nbsp;&nbsp;&nbsp;}  
> &nbsp;&nbsp;&nbsp;&nbsp;chain forward {  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# No forwarding, drop everything  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type filter hook forward priority 0; policy drop;  
> &nbsp;&nbsp;&nbsp;&nbsp;}      
> &nbsp;&nbsp;&nbsp;&nbsp;chain output {  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Accept every outgoing connection by default  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type filter hook output priority 0; policy accept;       
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Drop connections to banned IPs  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\#ip daddr \$BLOCKED_IPS drop  
> &nbsp;&nbsp;&nbsp;&nbsp;}   
> }  

## 21. Enable services
Activate services that should run at startup via

```shell
$ arch-chroot /mnt systemctl disable sddm
$ arch-chroot /mnt systemctl enable {bluetooth,cpupower,cronie,cups,sddm-plymouth,systemd-timesyncd,clamav-daemon,clamav-freshclam,clamav-clamonacc,clamav-unofficial-sigs,dhcpcd,dnscrypt-proxy,NetworkManager,nftables,reflector,thermald}
$ arch-chroot /mnt sh -c 'sudo -Hu <USERNAME> systemctl --user enable {pipewire,pipewire-pulse,pipewire-media-session}'
```                                  

### 22. Update databases  
You might want to run the following commands to speed up a few things:

```shell
$ arch-chroot /mnt pkgfile --update  
$ arch-chroot /mnt updatedb  
```

## 23. Update Notifications
If you want to regulary check for updates and display a notification on your desktop you can do so by creating a script at `/mnt/etc/cron.daily/check_updates.sh` by running

```shell
$ wget -O "/mnt/etc/cron.daily/check_updates.sh" "https://gitlab.com/marcjose/archlinux/installation-guide/-/raw/main/scripts/check_updates.sh"
```
and making it executable with `chmod +x "/mnt/etc/cron.daily/check_updates.sh"`. 

## 24. Finishing
Before restarting we have to close and unmount everything by running 

```shell
$ swapoff /mnt/swap  
$ umount -R /mnt  
$ cryptsetup close archlinux  
``` 

Now you can reboot into your new system with `reboot now` :)  
